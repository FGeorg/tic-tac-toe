import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import model.Model;


public class Main extends Application {

    private Timer timer;
    private boolean turnX = true;

    @Override
    public void start(Stage stage) throws Exception {

        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);


        Group group = new Group();
        group.getChildren().add(canvas);

        Scene scene = new Scene(group);

        stage.setTitle("TicTacToe von Florian");

        stage.setScene(scene);
        stage.show();

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Model model = new Model();
        Graphics graphics = new Graphics(model, gc);
        timer = new Timer(model, graphics);
        timer.start();


        InputHandler inputHandler = new InputHandler(model);

        scene.setOnMouseClicked(mouseEvent -> inputHandler.onClick(mouseEvent));



    }
}
