import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import model.Model;

public class Graphics {
    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }


    public void draw() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        gc.setLineWidth(2.0);
        gc.strokeLine(0, 200, 600, 200);
        gc.strokeLine(0, 400, 600, 400);
        gc.strokeLine(200, 0, 200, 600);
        gc.strokeLine(400, 0, 400, 600);
        drawXAndO(model.getChars());
    }


    private void drawXAndO(char[][] chars) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (chars[i][j] == 'X') {
                    gc.setFont(Font.font(72));
                    gc.strokeText("X", (i * 200) + 75, (j * 200) + 125);
                }
                if (chars[i][j] == 'O') {
                    gc.setFont(Font.font(72));
                    gc.strokeText("O", (i * 200) + 75, (j * 200) + 125);
                }
            }

        }
    }



}
