import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import model.Model;

public class InputHandler extends StackPane {
    private Model model;


    public InputHandler(Model model) {
        this.model = model;
    }


    public void onClick(MouseEvent mouseEvent) {
        char[][] stateBoard = model.getChars();
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {

            if (mouseEvent.getX() < 200 && mouseEvent.getY() < 200) {
                stateBoard[0][0] = 'X';
            } else if (mouseEvent.getX() < 400 && mouseEvent.getY() < 200) {
                stateBoard[1][0] = 'X';
            } else if (mouseEvent.getX() < 600 && mouseEvent.getY() < 200) {
                stateBoard[2][0] = 'X';
            } else if (mouseEvent.getX() < 200 && mouseEvent.getY() < 400) {
                stateBoard[0][1] = 'X';
            } else if (mouseEvent.getX() < 400 && mouseEvent.getY() < 400) {
                stateBoard[1][1] = 'X';
            } else if (mouseEvent.getX() < 600 && mouseEvent.getY() < 400) {
                stateBoard[2][1] = 'X';
            } else if (mouseEvent.getX() < 200 && mouseEvent.getY() < 600) {
                stateBoard[0][2] = 'X';
            } else if (mouseEvent.getX() < 400 && mouseEvent.getY() < 600) {
                stateBoard[1][2] = 'X';
            } else if (mouseEvent.getX() < 600 && mouseEvent.getY() < 600) {
                stateBoard[2][2] = 'X';
            }
        }

        if (mouseEvent.getButton() == MouseButton.SECONDARY) {

            if (mouseEvent.getX() < 200 && mouseEvent.getY() < 200) {
                stateBoard[0][0] = 'O';
            } else if (mouseEvent.getX() < 400 && mouseEvent.getY() < 200) {
                stateBoard[1][0] = 'O';
            } else if (mouseEvent.getX() < 600 && mouseEvent.getY() < 200) {
                stateBoard[2][0] = 'O';
            } else if (mouseEvent.getX() < 200 && mouseEvent.getY() < 400) {
                stateBoard[0][1] = 'O';
            } else if (mouseEvent.getX() < 400 && mouseEvent.getY() < 400) {
                stateBoard[1][1] = 'O';
            } else if (mouseEvent.getX() < 600 && mouseEvent.getY() < 400) {
                stateBoard[2][1] = 'O';
            } else if (mouseEvent.getX() < 200 && mouseEvent.getY() < 600) {
                stateBoard[0][2] = 'O';
            } else if (mouseEvent.getX() < 400 && mouseEvent.getY() < 600) {
                stateBoard[1][2] = 'O';
            } else if (mouseEvent.getX() < 600 && mouseEvent.getY() < 600) {
                stateBoard[2][2] = 'O';
            }
        }

    }


}
