package model;

public class Model {
    public static final int WIDTH = 600;
    public static final int HEIGHT = 600;
    private char[][] chars = new char[3][3];


    public void setChars(char[][] chars) {
        this.chars = chars;
    }

    public char[][] getChars() {
        return chars;
    }

    public Model() {
    }

    public static int getWIDTH() {
        return WIDTH;
    }

    public static int getHEIGHT() {
        return HEIGHT;
    }
}
